#' Imports emme format file to data frame
#'
#' Imports emme OD file, with columns 'Origin', 'Destination', 'Trips' and a
#' header of size skiplines.  Outputs data as data frame.
#'
#' @param inputFileName file name of input - including path if different to
#'   working directory
#' @param skiplines number of lines in header.  Default 4.
#' @param columns names of columns in input file. Dafault "Origin", "Destination", "Trips"
#' @param numericZones bool for whether origin and destination zoning designations are numeric.
#'   May get NAs in destination column if this is inappropriately set to true.  Default false.
#' @return Data frame with three columns
#'
#' @example example <- importEmme(system.file("extData", "example_OD.txt", package = "EmmeIO"), skiplines = 3)

importEmme <- function(inputFileName, skiplines = 4,
                       columns = c("Origin", "Destination", "Trips"),
                       numericZones = FALSE) {
  input <- read.table(inputFileName, header = FALSE, skip = skiplines)

  if(ncol(input)==2) {
    input <- data.frame(Origin = input$V1,
                        str_split_fixed(input$V2, ":", n = 2),
                        stringsAsFactors = FALSE)
  } else {
    # remove colons from end of column 2
    input$V2 <- gsub(':','',input$V2)
  }

  names(input) <- columns

  input[,3] <- as.numeric(input[,3])

  # will not work if destination contains text - this is fine.
  if(numericZones)  input[,2] <- as.numeric(input[,2])

  return(input)
}

#' Imports multiple emme format file to data frame
#'
#' Imports multiple emme OD file and outputs either a list of data frames,
#' or a single data frame with additional column of emme file name
#'
#' @param inputFileNames file names of input - including path if different to
#'   working directory
#' @param skiplines number of lines in header, a vector of length(inputFileNames)
#'  if different. Default to rep(4, inputFileNames).
#' @param columns names of columns in input file. Assumes all files have same
#' columns. Dafault "Origin", "Destination", "Trips"
#' @param dataframe number of lines in header, a vector of length(inputFileNames)
#'  if different. Default to TRUE.
#' @param rem_zeros bool, whether to remove rows which has zero 'trips' or keep them. Default to TRUE.
#' @return Data frame with columns specified, 'FileName',
#' if dataframe is set to FALSE, a list of data frames is returned
#' with 'Origin', 'Destination', 'Trips' columns.
#'
#' @examples
#' filename1 <- system.file("extData", "example_OD.txt", package = "EmmeIO")
#' filename2 <- system.file("extData", "example_OD_2.txt", package = "EmmeIO")
#' example <- importEmmeMulti(c(filename1, filename2), skiplines = c(3, 4))

importEmmeMulti <- function(inputFileNames,
                            skiplines = rep(4, length(inputFileNames)),
                            columns = c("Origin", "Destination", "Trips"),
                            dataframe = TRUE,
                            rem_zeros = TRUE) {

  # error check
  if(length(inputFileNames) != length(skiplines))
    stop("Please provide same length for inputFileNames and skiplines inputs")

  # import all using importEmme
  input <- lapply(1:length(inputFileNames), function(i) {
                  temp <- importEmme(inputFileNames[i], skiplines[i])
                  if(rem_zeros) temp <- subset(temp, Trips != 0)
                  temp <- importEmme(inputFileNames[i], skiplines[i], columns)
                  if(dataframe) temp$FileName <- inputFileNames[i]
                  return(temp)
                  })
  # name the list
  if(!dataframe) names(input) <- inputFileNames

  # rbind all data frames
  if(dataframe) input <- do.call(rbind, input)

  return(input)
}

#' Exports data frame for OD matrix as emme formatted file
#'
#' Takes data frame with OD matrix, outputs as emme formatted file, with filename source_sub[1]_sub[2]...[ext]
#'
#' @param input input data frame
#' @param outputFolder folder for output to be saved in
#' @param source source of data - for output file naming. Default to NULL
#' @param columns names of columns of data frame to be output.  Default assumes OD matrix
#'   with columns 'Origin', 'Destination', 'Trips'
#' @param sub subset of input to be used, e.g. Mode = 'LGV', in form of
#'   list.  Default all
#' @param add if !NULL add data frame with same 3 columns as input.  Orders
#'   based on first and second columns.  Default NULL
#' @param emmeHeader header for emme file.  Default labels matrix as mf001
#' @param ext output file extension.  Default ".txt"
#' @param space bool, defines whether there is a space between the final two
#'   columns. Needs to be FALSE when using MUD.exe.  Default TRUE
#'
#' @examples
#' data(example)
#' exportEmme(x, '.', "example", ext = ".dat")
#' exportEmme(y, '.', "example", columns = c("X", "Y", "Trips"),
#'   sub = list(Mode = 'Car', TimePeriod = 'AM'),
#'   emmeHeader = "t matrices\nd matrix=mf001\na matrix=mf001 example\tMode Car TimePeriod AM Matrix'")
exportEmme <- function(input, outputFolder,
					             source = NULL,
                       columns = c("Origin", "Destination", "Trips"),
                       sub = list(),
                       add = NULL,
                       emmeHeader = "t matrices\nd matrix= mf001\na matrix= mf001 temp1 0 ",
                       ext = ".txt",
                       space = TRUE) {

  # Subset the input based on sub list argument
  # Names of the sub list should be column names in input
  if(length(sub) > 0) {
    for(i in 1:length(sub)) {
      input <- input[input[,names(sub)[i]] == sub[[i]],]
    }
  }

  # Once subset is defined, select the 3 columns needed
  input <- input[,columns]

  # Add a data frame with the same 3 columns if it doesn't already exist
  # And order it based on the first and second origin/destination columns
  if(!is.null(add)) {
    input <- do.call(rbind, list(input, add))
    if(!any(grepl("^[A-Za-z]+$", input[,1], perl = T))) input[,1] <- as.numeric(input[,1])
    if(!any(grepl("^[A-Za-z]+$", input[,2], perl = T))) input[,2] <- as.numeric(input[,2])
    input <- input[order(input[,1], input[,2]),]
  }


  # Create an emme format column
  if(space == TRUE) {
    if(nrow(input) > 0) input <- data.frame(All = paste0(" ", input[,1], " ", input[,2], ": ", input[,3]),
                                            stringsAsFactors = FALSE)
  } else {
    if(nrow(input) > 0) input <- data.frame(All = paste0(" ", input[,1], " ", input[,2], ":", input[,3]),
                                            stringsAsFactors = FALSE)
  }

  # Open file connection and write the header the final emme format column
  # Name of the file connection depends on the sub list argument defined
  output <- file(paste0(outputFolder, "\\",
                        source,
                        if(!is.null(source) & length(sub) > 0) "_",
                        if(length(sub) > 0) paste(as.vector(sub), collapse = "_"),
                        ext))
  if(nrow(input) > 0) writeLines(paste0(emmeHeader, "\n", paste(input$All, collapse = "\n")),
                                 output)
  if(nrow(input) == 0) writeLines(emmeHeader, output)
  close(output)

}
