#' Checks if the matrix includes zones which are not in the list of input zones
#'
#' @param data data frame with columns including cols
#' @param zones a vector of unique zones to be checked against
#' @param columns names of columns which represent zones. Dafault "Origin", "Destination"
#' @return error message if there it is not a match

checkZoning <- function(data, zones, columns = c("Origin", "Destination")) {

  checks <- select(data, columns) %>% apply(2, function(i) any(!unique(i) %in% zones))
  if(any(checks)) stop("Your data includes zones which is not in your list of zones")

}
