#' Adds or remove an ensemble name to the origin and destination columns
#' of a matrix
#'
#' Imports one or more emme OD file and outputs the files
#' with ensemble added/removed
#'
#' @param inputFileNames file names of input - including path if different to
#'   working directory
#' @param outputFolder folder for output to be saved in
#' @param skiplines number of lines in header, a vector of length(inputFileNames)
#'  if different. Default to rep(4, inputFileNames).
#' @param ensemble the name of the ensemble to be added/removed. Default to "ga"
#' @param add true/false whether ensemble is to be added. Default to TRUE
#' @param rem opposite to add. Default to !add
#' @param ... other parameters
#' @return files exported with ensemble added/removed.
#'

addEnsembleNaming <- function(inputFileNames, outputFolder,
															skiplines = rep(4, length(inputFileNames)),
															ensemble = "ga", add = TRUE, rem = !add, ...) {

	# import files
	if(length(inputFileNames) == 1) {
			input <- importEmme(inputFileNames, skiplines)
	} else if(length(input) > 1) {
			input <- importEmmeMulti(inputFileNames, skiplines)
	}

  # add or remove ensemble code
	if(add) {
		input$Origin <- paste0(ensemble, input$Origin)
		input$Destination <- paste0(ensemble, input$Destination)
	} else if(rem) {
		input$Origin <- gsub(ensemble, "", input$Origin)
		input$Destination <- gsub(ensemble, "", input$Destination)
	}

	# export emme files with ensemble added or removed
	if(length(inputFileNames) > 1) {
		for(i in unique(input$FileName)) {
			exportEmme(input, outputFolder, sub = list(FileName = i), ...)
		}
	} else if(length(inputFileNames) == 1) {
			exportEmme(input, outputFolder, ...)
	}

}
